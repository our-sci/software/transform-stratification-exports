Transform stratification output json to Soil App data model

```sh
# git clone this project repo
git clone git@gitlab.com:our-sci/transform-stratification-exports.git
cd transform-stratification-exports

# Add files to be transformed into a folder `input`
mkdir input
cp ~/your/files/*.json input/

yarn # or npm install
# Add files to folder in current 
yarn transform # or npm run transform

# Review results in `output` folder
ls ./output
```
