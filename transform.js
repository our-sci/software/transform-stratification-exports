// import * as fs from 'fs';
// import ObjectId from 'bson-objectid';
const fs = require('fs');
const ObjectId = require('bson-objectid');


const INPUT_DIR = './input';
const OUTPUT_DIR = './output'

function processFile(file) {
	const areaId = new ObjectId().toHexString();
	const stratificationId = new ObjectId().toHexString();
	const locationCollectionId = new ObjectId().toHexString();
	const locations = file.locationCollections.features.map(l => ({
		...l,
		id: new ObjectId().toHexString(),
		properties: {},
	}))
	return {
		...file,
		producers: file.producers.map(p => ({ ...p, fields: [p.fields]})),
		areas: [{
			...file.areas,
			id: areaId,
			properties: {
				name: "Field Boundary",
				featureOfInterest: file.fields[0].id,
				dateCreated: new Date().toISOString(),
				creator: '',
			},
		}],
		fields: [{
			...file.fields[0],
			address: {},
			contactPoint: [],
			area: [areaId],
			name: 'Field',
		}],
		stratifications: [{
			id: stratificationId,
			object: areaId,
			name: 'Stratification',
		}],
		locationCollections: [{
			id: locationCollectionId,
			object: areaId,
			resultOf: stratificationId,
			featureOfInterest: file.fields[0].id,
			features: locations.map(l => l.id),
		}],
		locations,
	}
}

async function main() {
	let fileNames;
	let files;
	try {
		fileNames = (await fs.promises.readdir(INPUT_DIR)).filter((fn) => fn.toLowerCase().endsWith('.json'));
	} catch (err) {
		console.warn('error reading files', err);
		return;
	}

	!fileNames.length && console.warn('no files to transform');

	try {
		// const read = (f) => fs.promises.readFile(f, 'utf8');
		files = (await Promise.all(
			fileNames.map(async (f) => fs.promises.readFile(`./${INPUT_DIR}/${f}`, 'utf8'))
		));
		// console.log(files);
	} catch (err) {
		console.warn('error reading files',err);
	}

	let parsedFiles;

	try {
		parsedFiles = files.map(f => JSON.parse(f));
	} catch(err) {
		console.log('error parsing files', err);
	}

	// console.log(parsedFiles.slice(0, 1));
	// const result = [parsedFiles[0]].map(processFile);
	const result = parsedFiles.map(processFile);
	// // console.log(JSON.stringify(result, null, '  '));
	// console.log(result[0].producers);
	// console.log(result[0].fields);
	// console.log(result[0].areas);
	// // console.log(result[0].fields);

	try {
		if(!fs.existsSync(OUTPUT_DIR)) {
			fs.mkdirSync(OUTPUT_DIR);
		}
		result.forEach(r => {
			const filename = Buffer.from(r.fields[0].id, 'utf8').toString('base64');
			fs.writeFileSync(
				`./${OUTPUT_DIR}/${filename}.json`, 
				JSON.stringify(r), 
				'utf8'
			);
			console.log(`wrote file ${filename}.json`);
		});
	} catch (err) {
		console.warn('error writing files', err);
	}

	const combined = result.reduce((r, x) => {
		return { 
			...Object.keys(x)
				.map(k => ({
					[k]: [
						...(r[k] ? r[k] : []),
						...x[k]
					],
				}))
				.reduce((acc, y) => ({ ...acc, ...y }),{}),
		}
	}, {});

	try {
		const filename = `combined_${Date.now()}.json`;
		fs.writeFileSync(
			`./${OUTPUT_DIR}/${filename}`,
			JSON.stringify({ 
				...combined, 
				projects: [ combined.projects[0] ],
				fields: combined.fields.map((f, i) => ({ ...f, name: `Field ${i}`})),
				// areas: combined.areas.map((f, i) => ({ ...f, name: `Area ${i}`})),
			}),
			'utf8'
		);
		console.log(`wrote file ${filename}`);

	} catch (err) {
		console.warn('error writing combined', err);
	}
}

main();

// Projects: 
// contact point? 
// object?: array of fields in project

// Producers
// fields should be an array

// Areas
// top level object should be an array, not an object
// id != field.id: areas are distinct from fields, IDs should not be reused
// properties.name? should have a human readable name for reference
// properties.featureOfInterest: back-reference to field

// Fields
// address?
// name: should have human readable name, even if it's just 'Field 1'
// contact?
// area: an array of the areas for the field

// Stratifications
// Top level key does not exist...
// id
// object
// name?

// LocationCollections: this should be an array
// id: each location collection needs a unique id
// remap from geojson featurecollection to LocationCollection, with references
//  to Location IDs as array in `member` field 

// Locations: create from LocationCollection `features` field
//